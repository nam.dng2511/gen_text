SPACE = ' '
LATIN_CHARACTERS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ:#./-'
KANJI_CHARACTERS = ''.join([line.rstrip('\n') for line in open('kanji.txt')])
KATAKANA_CHARS = ''.join(set(''.join([line.rstrip('\n') for line in open('katakana.txt')])))
HIRAGANA_CHARS = ''.join(set(''.join([line.rstrip('\n') for line in open('hiragana.txt')])))
# katakana = [line.rstrip('\n') for line in open('katakana.txt')]
# hiragana = [line.rstrip('\n') for line in open('hiragana.txt')]

print('TOTAL NUMBER OF CHARACTERS: ', len(SPACE) + len(KANJI_CHARACTERS) + len(KATAKANA_CHARS) + len(HIRAGANA_CHARS) + len(LATIN_CHARACTERS))

# if __name__== "__main__" :
#     f = open('char_list.txt', 'a')
#     f.write(SPACE + KANJI_CHARACTERS + KATAKANA_CHARS + HIRAGANA_CHARS + LATIN_CHARACTERS)
#     f.close()
