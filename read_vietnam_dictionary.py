import json
import numpy as np

with open('vn_json.txt', 'r') as f_read:
    lines = f_read.readlines()

all_words = []

for l in lines:
    l_dict = json.loads(l)
    words = l_dict['text'].split()

    odd_capitalize = np.random.uniform()
    for word in words:
        if word not in all_words:
            if odd_capitalize < 0.4:
                all_words.extend([word, word.upper()])
            else: 
                all_words.append(word)

with open('momo/momo_vietnam_all.txt', 'w') as f_write:
    for word in all_words:
        f_write.write(word + '\n')
