katakana_txt = open("hiragana.txt","w", encoding='utf8') 

if __name__== "__main__" :
    katakana_prefix = [r'\u30A', r'\u30B', r'\u30C', r'\u30D', r'\u30E', r'\u30F']
    hiragana_prefix = [r'\u304', r'\u305', r'\u306', r'\u307', r'\u308', r'\u309']
    hexa_suffix = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']

    for i in range(16*6):
        k = chr(0x3040+i)
        print(k)
        katakana_txt.write(k + "\n")

    katakana_txt.close()
