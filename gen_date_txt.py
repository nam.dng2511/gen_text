import random
from datetime import datetime, date
import time
import numpy as np
import math
import string

file1 = open("battery/battery_21_03.txt","w")

DELIMINATORS = ['/', '-', '.', '', 'japan']
# DELIMINATORS = ['/', '-']
DELI_DICT = {
    '/': 1,
    '-': 0,
    '.': 0,
}
NUM_DICT = {
    0: 0.05,
    1: 0.4,
    2: 0.05,
    3: 0.05,
    4: 0.05,
    5: 0.05,
    6: 0.05,
    7: 0.2,
    8: 0.05,
    9: 0.05,
}
english_words = [line.rstrip('\n') for line in open('words_alpha.txt') if len(line) > 4 and len(line) < 12]
random.shuffle(english_words)
kanji = [line.rstrip('\n') for line in open('kanji.txt')]
katakana = [line.rstrip('\n') for line in open('katakana.txt')]
hiragana = [line.rstrip('\n') for line in open('hiragana.txt')]
JPN_ALPHABETS = [kanji, katakana, hiragana]
JPN_DELI = ['年', '月', '日']
PREFIXES = ['消費期限', '賞味期限']
UPPERCASE_CHAR_AND_NUMBER = [char for char in '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ']

def random_true_false():
    return True if np.random.choice(2) == 1 else False

def gen_jpn_word(jpn_chars):
    char_index_list = np.random.choice(len(jpn_chars), np.random.randint(3,6))
    char_list = np.array(jpn_chars)[char_index_list]
    return ''.join(char_list)

def id_generator():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=random.choice(np.arange(6,9))))

def gen_date_format(deliminator, space):
    YEAR_FORMATS = ['%y', '%Y']
    MONTH_FORMATS = ['%-m', '%m', '%b']
    DAY_FORMATS = ['%-d', '%d', '']
    year_format = YEAR_FORMATS[np.random.choice(len(YEAR_FORMATS))]
    month_format = MONTH_FORMATS[np.random.choice(len(MONTH_FORMATS))]
    day_format = DAY_FORMATS[np.random.choice(len(DAY_FORMATS))]
    if deliminator == 'japan':
        if day_format == '':
            return year_format + JPN_DELI[0] + space + month_format + JPN_DELI[1] + space

        return year_format + JPN_DELI[0] + space + month_format + JPN_DELI[1] + space + day_format + JPN_DELI[2] 

    if day_format == '':
        return year_format + deliminator + space + month_format

    return year_format + deliminator + space + month_format + deliminator + space + day_format

def gen_ful_date():
    #deliminator and space
    deliminator = DELIMINATORS[np.random.choice(len(DELIMINATORS))]
    space_bool = random_true_false()
    space = ' ' if deliminator in ['.', '/'] and space_bool else ''
    # space = ' '

    #prefix
    prefix = ''
    prefix_bool = random_true_false()
    if prefix_bool:
        prefix = PREFIXES[np.random.choice(len(PREFIXES))]
        colon_bool = random_true_false()
        prefix = prefix + ':' if colon_bool else prefix
        prefix = prefix + space

    #date
    start_time = '01/01/1900'
    end_time = '31/12/2050'
    start_ts = time.mktime(datetime.strptime(start_time, "%d/%m/%Y").timetuple())
    end_ts = time.mktime(datetime.strptime(end_time, "%d/%m/%Y").timetuple())
    d = random.randint(int(start_ts), int(end_ts))
    date_str = date.fromtimestamp(d).strftime(gen_date_format(deliminator, space))
    date_str = date_str.upper()

    #surfix
    surfix_bool = random_true_false()
    surfix = ''
    if surfix_bool:
        SURFIX_ONLY_DELI = ['.', '-', '/']
        if deliminator in SURFIX_ONLY_DELI:
            SURFIX_ONLY_DELI.remove(deliminator)
        surfix_deli = SURFIX_ONLY_DELI[np.random.choice(len(SURFIX_ONLY_DELI))]
        surfix_code = ''.join([UPPERCASE_CHAR_AND_NUMBER[i] for i in np.random.choice(len(UPPERCASE_CHAR_AND_NUMBER), 2)])
        surfix = surfix_deli + surfix_code

    return prefix + date_str + surfix

def gen_string_date_like():
    # gen non-jpn date
    MAX_REP = 2
    deliminator = DELIMINATORS[np.random.choice(np.arange(0, len(DELIMINATORS)), p=list(DELI_DICT.values()))]
    random_num_list = np.random.choice(10, np.random.randint(4,9), p=list(NUM_DICT.values()))
    random_num_list = [str(i) for i in random_num_list]
    position = np.random.choice(np.arange(1, len(random_num_list)), np.random.choice(MAX_REP+1, p=[0.1, 0.1, 0.8]), replace=False)
    date_like_string = np.insert(random_num_list, position, deliminator)
    date_like_string = ''.join(date_like_string)

    # gen jpn date
    position = np.random.choice(np.arange(1, len(random_num_list) + 1), np.random.choice([2,3]), replace=False)
    random.shuffle(JPN_DELI)
    jpn_date = random_num_list.copy()
    jpn_date = np.insert(jpn_date, position, JPN_DELI[0: len(position)])
    jpn_date = ''.join(jpn_date)

    # return np.random.choice([date_like_string, jpn_date], p=[0.5, 0.5])
    return date_like_string + ' ' + jpn_date

def random_date_num_text(index):
    date_str = gen_ful_date()
    # date_str = gen_string_date_like()

    # add word from library
    text = english_words[index % len(english_words)]
    # indices = np.random.choice(np.arange(0, len(text) + 1), np.random.choice(math.ceil(len(text) / 2) + 1), replace=False)
    # upper_lower_text = "".join(c.upper() if i in indices else c for i, c in enumerate(text))
    upper_case_bool = random_true_false()
    upper_lower_text = text.upper() if upper_case_bool else text.lower()

    # add id string
    # id = id_generator()

    # add japanese text
    alphabet_list = JPN_ALPHABETS[np.random.choice(len(JPN_ALPHABETS))]
    jpn_text = gen_jpn_word(alphabet_list)

    # serial number
    serial_number = ''.join([str(i) for i in np.random.choice(np.arange(10), np.random.choice(list(range(1,11))))])

    ### code (upper case character + number)
    code = ''.join([UPPERCASE_CHAR_AND_NUMBER[i] for i in np.random.choice(len(UPPERCASE_CHAR_AND_NUMBER), np.random.choice(list(range(2,11))))])
    # add deliminator to code
    code_deli = np.random.choice(['', '-'], p=[0.8, 0.2])
    print(code)
    position = np.random.choice(list(range(1,len(code))))
    code = ''.join(np.insert([str(i) for i in code], position, code_deli))

    # special_cases = np.random.choice(['Ni-Cd', 'Li-ion', 'Ni-MH'])
    special_cases = 'Ni-MH'

    # # kilogram values
    # units = ['kg', 'Kg', 'KG', 'kgs', 'Kgs']
    # is_float = np.random.uniform() < 0.5
    # value = str(np.random.randint(1, 999) / 10 if is_float else np.random.randint(1, 99))
    # kg_value = value + np.random.choice(units)

    #5/2/3 // #2(date)/3(ABC)/3(JPN)/1(Serialnumber)/1(CODE-SN201 minlen=1 maxlen=10)
    #YEAR (1900-2050)
    choices = [upper_lower_text, jpn_text, serial_number, code, special_cases]
    
    return choices[np.random.choice(len(choices), p=[0.2, 0.2, 0.2, 0.2, 0.2])]
    # return code
    # return date_str + ' ' + jpn_text + ' ' + upper_lower_text

CHARACTERS = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
NUMBERS = list('0123456789')
TEMPLATE = ['AJ0102', '19/12/05-01', '2007/12/05-01', '1000-K', 'A1']

def random_text_toyota():
    template = np.random.choice(TEMPLATE, p=[0.6, 0.1, 0.1, 0.1, 0.1])
    generated_text = []
    for char in list(template):
        if char in CHARACTERS:
            generated_text.append(random.choice(CHARACTERS))
        elif char in NUMBERS:
            generated_text.append(random.choice(NUMBERS))
        else:
            generated_text.append(char)

    return ''.join(generated_text)        
    
if __name__== "__main__" :
    for i in range(0, 5000):
        print(random_date_num_text(i))
        file1.write(random_date_num_text(i))
        file1.write('\n')

    file1.close()

    # gen_string_date_format_like()

    # random_date_num_text()